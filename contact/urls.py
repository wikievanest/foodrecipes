from django.contrib import admin
from django.urls import path

from .views import emailView, successView

app_name = 'contact'

urlpatterns = [
    path('', emailView, name='email'),
    path('success/', successView, name='success')
]
