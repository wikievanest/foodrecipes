from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import ContactForm

def emailView(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        
        if form.is_valid():
            name = form.cleaned_data['name']
            subject = form.cleaned_data['subject']
            to_email = form.cleaned_data['from_email']
            subject = name +' - ' + to_email + ' - ' + subject
            message = form.cleaned_data['message']
            try:
                email = EmailMessage(subject,message,'allfoodr@gmail.com',['support@allfoodrecipes.net'])
                email.send()
                messages.add_message(request, messages.INFO, '<strong>Thank you for getting in touch!</strong><br>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!')
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            
            return redirect('/contact')
    return render(request, "contact/email.html", {'form': form, "title": "Contact Us"})

def successView(request):
    return HttpResponse('Success! Thank you for your message.')