from django.db import models
from django.utils.text import slugify
from django.urls import reverse
from datetime import datetime
# Create your models here.

class Recipe(models.Model):
    rid = models.CharField(max_length=50, unique=True, null=True, blank=True)
    title = models.CharField(max_length=200, null=True, blank=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    link = models.CharField(max_length=255, null=True, blank=True)
    author = models.CharField(max_length=100, null=True, blank=True)
    categories = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    keywords = models.TextField(blank=True, null=True)
    pubdate = models.DateTimeField()
    image_url = models.CharField(max_length=255, null=True, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    published = models.IntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)

    def __str__(self):
        return self.title

    def categories_as_list(self):
        return self.categories.split(',')

    class Meta:
        db_table = 'recipes'
        managed = True
        verbose_name = 'product'
        verbose_name_plural = 'products'

    def get_absolute_url(self):
        # Slugify the combination of role and company_name as these may contain
        # whitespace or other characters that are not permitted in urls.
        slug = slugify(f"{self.title}")
        return reverse("detail", kwargs={"slug": slug})

    def get_url(self):
        return reverse("track", kwargs={"url": self.url})

class PinAccount(models.Model):
    first_name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    password = models.CharField(max_length=50, null=True, blank=True)
    level = models.CharField(max_length=50, null=True, blank=True)
    username = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.first_name

    class Meta:
        db_table = 'pin_accounts'
        managed = True
        verbose_name = 'pin_account'
        verbose_name_plural = 'pin_accounts'

class Pin(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, unique=True, null=True, blank=True)
    published = models.IntegerField(default=0)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'pins'
        managed = True
        verbose_name = 'pins'
        verbose_name_plural = 'pins'

class Repin1(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    published = models.IntegerField(default=0)
    pin_main_id = models.CharField(max_length=50, null=True, blank=True)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        db_table = 'repins1'
        managed = True
        verbose_name = 'repins1'
        verbose_name_plural = 'repins1'

class Repin2(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    published = models.IntegerField(default=0)
    pin_main_id = models.CharField(max_length=50, null=True, blank=True)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        db_table = 'repins2'
        managed = True
        verbose_name = 'repins2'
        verbose_name_plural = 'repins2'

class Repin3(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    published = models.IntegerField(default=0)
    pin_main_id = models.CharField(max_length=50, null=True, blank=True)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        db_table = 'repins3'
        managed = True
        verbose_name = 'repins3'
        verbose_name_plural = 'repins3'

class Repin4(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    published = models.IntegerField(null=True, blank=True, default=0)
    pin_main_id = models.CharField(max_length=50, null=True, blank=True)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        db_table = 'repins4'
        managed = True
        verbose_name = 'repins4'
        verbose_name_plural = 'repins4'

class Repin5(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    categories = models.TextField(blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=255, blank=True)
    image_path = models.CharField(max_length=255, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    published = models.IntegerField(null=True, blank=True, default=0)
    pin_main_id = models.CharField(max_length=50, null=True, blank=True)
    pin_id = models.CharField(max_length=50, unique=True, null=True, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        db_table = 'repins5'
        managed = True
        verbose_name = 'repins5'
        verbose_name_plural = 'repins5'

class Comment(models.Model):
    comment_id = models.CharField(max_length=50, null=True, blank=True)
    account_id = models.CharField(max_length=50, null=True, blank=True)
    username =  models.CharField(max_length=50, null=True, blank=True)
    text = models.TextField(blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'comments'
        managed = True
        verbose_name = 'comment'
        verbose_name_plural = 'comments'

class PinComment(models.Model):
    account_id = models.CharField(max_length=50, unique=True, null=True, blank=True)
    pin_id = models.CharField(max_length=50, null=True, blank=True)
    comment_id = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.pin_id

    class Meta:
        db_table = 'pin_comments'
        managed = True
        verbose_name = 'pin_comment'
        verbose_name_plural = 'pin_comments'
    


class Pinner(models.Model):
    account_id = models.CharField(max_length=50, unique=True, null=True, blank=True)
    pinner_id = models.CharField(max_length=50, null=True, blank=True)
    follower_count = models.PositiveIntegerField(null=True, blank=True)
    username =  models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    start_follow_at = models.DateTimeField(auto_now_add=True, blank=True)
    follow_me = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.username

    class Meta:
        db_table = 'pinner'
        managed = True
        verbose_name = 'pinner'
        verbose_name_plural = 'pinners'

class keywords(models.Model):
    keyword = models.TextField(blank=True,null=True)

    def __str__(self):
        return self.keyword

    class Meta:
        db_table = 'keywords'
        managed = True
        verbose_name = 'keyword'
        verbose_name_plural = 'keywords'
    

