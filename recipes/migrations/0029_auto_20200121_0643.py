# Generated by Django 3.0.2 on 2020-01-20 23:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0028_keywords'),
    ]

    operations = [
        migrations.AlterField(
            model_name='keywords',
            name='keyword',
            field=models.TextField(blank=True, null=True),
        ),
    ]
