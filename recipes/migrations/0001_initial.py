# Generated by Django 3.0.1 on 2020-01-02 10:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=200, null=True)),
                ('link', models.CharField(blank=True, max_length=255, null=True)),
                ('author', models.CharField(blank=True, max_length=100, null=True)),
                ('categories', models.TextField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('keywords', models.TextField(blank=True, null=True)),
                ('pubdate', models.DateTimeField()),
                ('image_url', models.CharField(blank=True, max_length=255, null=True)),
                ('image_thumbnail', models.CharField(blank=True, max_length=255, null=True)),
                ('image_urls', models.CharField(blank=True, max_length=255, null=True)),
            ],
        ),
    ]
