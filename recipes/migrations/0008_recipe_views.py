# Generated by Django 3.0.1 on 2020-01-04 04:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0007_recipe_published'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='views',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
