from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from .models import Recipe
from django.db import connection
from .utils import str_to_list
import itertools



def index(request):
    recipe_list = Recipe.objects.filter(published=1).order_by('-pubdate')
    page = request.GET.get('page', 1)
    paginator = Paginator(recipe_list, 5)

    try:
        recipes = paginator.get_page(page)
    except PageNotAnInteger:
        recipes = paginator.page(1)
    except EmptyPage:
        recipes = paginator.page(paginator.num_pages)
    return render(request, 'recipes/index.html', {'recipes': recipes, 'title': 'Explore Food Recipes Idea'})

def detail(request, slug):
    recipe = get_object_or_404(Recipe, slug='/'+slug)

    counter = recipe.views + 1
    Recipe.objects.filter(pk=recipe.id).update(views=counter)

    return render(request, 'recipes/detail.html', {'recipe': recipe, 'title': recipe.title})

    

def track(request, slug):
    recipe = get_object_or_404(Recipe, slug=slug)
    counter = recipe.views + 1
    Recipe.objects.filter(pk=recipe.id).update(views=counter)

    if recipe:
        return redirect(recipe.link, permanent=True)


def tags(request):
    cursor = connection.cursor()
    cursor.execute("SELECT categories from recipes")
    result = cursor.fetchall()
    result_list = list(result)

    xtags_list = []
    for x in result_list:
        xtags_list.append(list(x))

    tag_str = list(itertools.chain.from_iterable(result_list))

    tags = []

    for x in tag_str:
        j = str_to_list(x)
        tags.append(j)


    tag_list = list(itertools.chain.from_iterable(tags))
    return render(request, 'recipes/tags.html', {'tags': tag_list, 'title':'Tags'})

def recipe_by_tag(request, tagname):
    search = tagname.replace('-',' ').capitalize()
    recipe_list = Recipe.objects.filter(categories__icontains=search).filter(published=1).order_by('-pubdate')

    page = request.GET.get('page', 1)
    paginator = Paginator(recipe_list, 5)

    try:
        recipes = paginator.get_page(page)
    except PageNotAnInteger:
        recipes = paginator.page(1)
    except EmptyPage:
        recipes = paginator.page(paginator.num_pages)
    return render(request, 'recipes/search_by_tags.html', {'recipes': recipes, 'keyword': search, 'title': 'Search by '+ search})

def search(request):
    keyword = request.GET.get('q', None)
    recipe_list = Recipe.objects.filter(categories__icontains=keyword).filter(published=1).order_by('-pubdate')
    page = request.GET.get('page', 1)
    paginator = Paginator(recipe_list, 5)

    try:
        recipes = paginator.get_page(page)
    except PageNotAnInteger:
        recipes = paginator.page(1)
    except EmptyPage:
        recipes = paginator.page(paginator.num_pages)

    return render(request, 'recipes/search_by_tags.html', {'recipes': recipes, 'keyword': keyword, 'title': "Search by "+keyword})

def about(request):
    return render(request, 'recipes/about.html', {'title': "About Us"})
    
def privacy(request):
    return render(request, 'recipes/privacy.html', {'title': "Privacy Policy"})

def disclaimer(request):
    return render(request, 'recipes/disclaimer.html', {'title': "Disclaimer"})

def terms(request):
    return render(request, 'recipes/tos.html', {'title': "Term and Conditions"})

def contact(request):
    return render(request, 'recipes/contact.html', {'title': "Contact Us"})
    
