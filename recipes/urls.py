from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path("recipe/<str:slug>", views.detail, name="detail"),
    path("track/<slug:slug>", views.track, name="track"),
    path('tags', views.tags, name='tags'),
    path("tags/<str:tagname>", views.recipe_by_tag, name="recipe_by_tag"),
    path("search", views.search, name="search"),
    path("about", views.about, name="about"),
    path("privacy", views.privacy, name="privacy"),
    path("disclaimer", views.disclaimer, name="disclaimer"),
    path("terms", views.terms, name="terms"),
]