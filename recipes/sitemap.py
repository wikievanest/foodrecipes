from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import Site

from django.urls import reverse
from .models import Recipe

class Static_Sitemap(Sitemap):

    priority = 1.0
    changefreq = 'daily'

    def items(self):
        return ['privacy', 'disclaimer']

    def location(self, item):
        return reverse(item)

class Recipe_Sitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='allfoodrecipes.net', name='allfoodrecipes.net')
        return super(Recipe_Sitemap, self).get_urls(site=site, **kwargs)


    def items(self):
        return Recipe.objects.all().order_by('-pubdate')

    # def location(self, obj):
    #     return obj.slug

    def lastmod(self, obj): 
        return obj.pubdate
