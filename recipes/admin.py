from django.contrib import admin
from .models import Recipe, PinAccount, Pin


class PinAccountAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name','email','username','level')

class PinAdmin(admin.ModelAdmin):
    list_display = ('title', 'account_id','published')

class RecipeAdmin(admin.ModelAdmin):
    list_display = ('title', 'categories', 'pubdate', 'published', 'views')


admin.site.register(Recipe, RecipeAdmin)
admin.site.register(PinAccount, PinAccountAdmin)
admin.site.register(Pin, PinAdmin)